google-recaptcha (1.3.0-2) unstable; urgency=medium

  * Team upload
  * Modernize PHPUnit syntax
  * Update debian/clean
  * Update Standards-Version to 4.7.2

 -- David Prévot <taffit@debian.org>  Thu, 06 Mar 2025 13:55:50 +0100

google-recaptcha (1.3.0-1) unstable; urgency=medium

  * Switch to mode=git since upstream gitattributes removed the test folder
  * Update gbp.conf filter
  * New upstream version 1.3.0
  * Drop phpunit 9 patches
  * Update examples patch

 -- William Desportes <williamdes@wdes.fr>  Fri, 16 Jun 2023 23:54:18 +0200

google-recaptcha (1.2.4-5) unstable; urgency=medium

  * Set PHP PEAR Team as Maintainer of google-recaptcha
  * Move this package to php-team/pear
  * Bump Standards-Version to 4.6.2

 -- William Desportes <williamdes@wdes.fr>  Fri, 23 Dec 2022 14:40:59 +0400

google-recaptcha (1.2.4-4) unstable; urgency=medium

  * Remove unused autoload test file
  * Bump Standards-Version to 4.6.1
  * Depend on dh-sequence-phpcomposer
  * Update d/copyright for Debian files
  * Relieve Matthias and Felipe from the "Uploaders" field of d/control
  * Install usr/share/pkg-php-tools overrides
  * Mark the package as "Multi-Arch: foreign"
  * Add php-curl to test dependencies

 -- William Desportes <williamdes@wdes.fr>  Sun, 15 May 2022 15:46:51 +0200

google-recaptcha (1.2.4-3) unstable; urgency=medium

  * remove DEB_BUILD_OPTIONS because debhelper 13 handles it
  * Add a Debian patch for phpunit type hints
  * Remove sed calls before and after tests
  * Remove phpab tests autoloader for autopkgtests

 -- William Desportes <williamdes@wdes.fr>  Sat, 02 Jan 2021 20:21:12 +0100

google-recaptcha (1.2.4-2) unstable; urgency=medium

  [ William Desportes ]
  * Bump debhelper-compat to 13
  * Install examples (lintian: package-does-not-install-examples)
  * Remove a whitespace (lintian: file-contains-trailing-whitespace)
  * Set debian branch to debian/latest (DEP-14)
  * Adapt to recent version of PHPUnit (9) (Closes: #978381)
  * Run "cme fix dpkg" and "wrap-and-sort"
  * Update Standards-Version to 4.5.1
  * Add myself to the uploaders
  * Update d/copyright
  * Add d/examples, d/clean and d/docs
  * Add autoload generation
  * Add a patch to fix examples/* autoload paths

  [ David Prévot ]
  * Generate static autoloader inside ReCaptcha/
  * Install package in its proper (Class)path

 -- William Desportes <williamdes@wdes.fr>  Fri, 01 Jan 2021 22:51:16 +0100

google-recaptcha (1.2.4-1) unstable; urgency=medium

  * New upstream version 1.2.4
  * Add d/upstream/metadata
  * Fix d/watch file template
  * Bump Standards-Version to 4.5.0
  * Add phpunit to build dependencies
  * Bump debhelper from old 10 to 12.
  * Support debian tests
  * Add tests to debian rules
  * Add commands to make tests compatible with next phpunit version
  * Add php-curl to Build dependencies to improve testing coverage

 -- William Desportes <williamdes@wdes.fr>  Fri, 24 Apr 2020 08:13:33 +0200

google-recaptcha (1.2.3-1) unstable; urgency=medium

  * New upstream version

 -- Matthias Blümel <debian@blaimi.de>  Tue, 29 Oct 2019 22:45:40 +0100

google-recaptcha (1.1.3-4) unstable; urgency=medium

  [ William Desportes ]
  * Fix Vcs-urls.
    Signed-off-by: William Desportes <williamdes@wdes.fr>
  * Add gitlab CI config file

  [ Matthias Blümel ]
  * lintian doesn't like long lines in debian/changelog

 -- Felipe Sateler <fsateler@debian.org>  Sat, 12 Oct 2019 12:52:15 -0300

google-recaptcha (1.1.3-3) unstable; urgency=medium

  * Add comment clarifying the licensing status of the php files.
    The header is the MIT blurb, but the license is BSD.
    This will be fixed by upstream in the next release

 -- Felipe Sateler <fsateler@debian.org>  Thu, 15 Aug 2019 11:03:42 -0400

google-recaptcha (1.1.3-2) unstable; urgency=medium

  [ Matthias Blümel ]
  * use architecture all instead of any

 -- Felipe Sateler <fsateler@debian.org>  Sun, 16 Jun 2019 09:46:13 -0400

google-recaptcha (1.1.3-1) unstable; urgency=medium

  * Initial release (Closes nothing)

 -- Felipe Sateler <fsateler@debian.org>  Tue, 01 May 2018 23:28:09 -0300
